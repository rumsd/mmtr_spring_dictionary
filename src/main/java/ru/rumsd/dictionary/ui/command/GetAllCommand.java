package ru.rumsd.dictionary.ui.command;

import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.dict.controller.DictionaryController;

@Component("getAllCommand")
public class GetAllCommand implements Command {
    private DictionaryController controller;

    public GetAllCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        controller.getAll();
    }

    @Override
    public String toString() {
        return "Отобразить все записи";
    }
}
