package ru.rumsd.dictionary.ui.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.App;
import ru.rumsd.dictionary.dict.controller.DictionaryController;
import ru.rumsd.dictionary.dict.model.Dictionary;
import ru.rumsd.dictionary.ui.Console;

@Component("startMenu")
public class SelectDictCommand implements Command {
    private DictionaryController controller;

    public SelectDictCommand(@Autowired DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        Console.writeString("Выберите словарь:");
        Console.writeString("1 - Словарь 5 цифр");
        Console.writeString("2 - Словарь 4 латинские буквы");
        while (true) {
            int num = Console.readInt();
            if (num == 1) {
                checkChangedBeforeSelect();
                controller.setDictionary((Dictionary) App.context.getBean("dictFiveDigit"));
                break;
            } else if (num == 2) {
                checkChangedBeforeSelect();
                controller.setDictionary((Dictionary) App.context.getBean("dictFourLetter"));
                break;
            } else {
                Console.writeString("Введите число 1 или 2");
            }
        }
    }

    private void checkChangedBeforeSelect() {
        if (controller.checkChanged() && Console.saveQuestion()){
            controller.save();
        }
    }

    @Override
    public String toString() {
        return "Выбрать словарь";
    }
}
