package ru.rumsd.dictionary.ui.command;

import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.dict.controller.DictionaryController;
import ru.rumsd.dictionary.ui.Console;

@Component("getCommand")
public class GetCommand implements Command {
    private DictionaryController controller;

    public GetCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        Console.writeString("Введите слово для поиска: ");
        String key = Console.readString();
        controller.get(key);
    }

    @Override
    public String toString() {
        return "Найти запись";
    }
}
