package ru.rumsd.dictionary.ui.command;

import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.dict.controller.DictionaryController;
import ru.rumsd.dictionary.ui.Console;

@Component("deleteCommand")
public class DeleteCommand implements Command {
    private DictionaryController controller;

    public DeleteCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        Console.writeString("Введить слово для удаления: ");
        String key = Console.readString();
        controller.delete(key);
    }

    @Override
    public String toString() {
        return "Удалить запись";
    }
}
