package ru.rumsd.dictionary.ui.command;

import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.dict.controller.DictionaryController;
import ru.rumsd.dictionary.ui.Console;

@Component("createCommand")
public class CreateCommand implements Command {
    private DictionaryController controller;

    public CreateCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        Console.writeString("Введите слово: ");
        String key = Console.readString();
        Console.writeString("Ведите значение слова: ");
        String value = Console.readString();
        controller.create(key, value);
    }

    @Override
    public String toString() {
        return "Создать новую запись";
    }
}
