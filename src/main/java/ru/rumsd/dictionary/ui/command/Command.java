package ru.rumsd.dictionary.ui.command;

public interface Command {
    void execute();
}
