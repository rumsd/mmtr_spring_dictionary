package ru.rumsd.dictionary.ui.command;

import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.dict.controller.DictionaryController;
import ru.rumsd.dictionary.ui.Console;

@Component("exitCommand")
public class ExitCommand implements Command {
    private DictionaryController controller;

    public ExitCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        if (controller.checkChanged()) {
            if (Console.saveQuestion()) {
                controller.save();
            }
        }
        System.exit(0);
    }

    @Override
    public String toString() {
        return "Выход";
    }
}
