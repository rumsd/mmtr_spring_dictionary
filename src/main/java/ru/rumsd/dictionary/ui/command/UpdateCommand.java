package ru.rumsd.dictionary.ui.command;

import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.dict.controller.DictionaryController;
import ru.rumsd.dictionary.ui.Console;

@Component("updateCommand")
public class UpdateCommand implements Command {
    private DictionaryController controller;

    public UpdateCommand(DictionaryController controller) {
        this.controller = controller;
    }

    @Override
    public void execute() {
        Console.writeString("Введите слово: ");
        String key = Console.readString();
        Console.writeString("Введите новое значение слова: ");
        String newValue = Console.readString();
        controller.update(key, newValue);
    }

    @Override
    public String toString() {
        return "Изменить запись";
    }
}
