package ru.rumsd.dictionary.ui;

import ru.rumsd.dictionary.ui.command.Command;

import java.util.Map;

public class Menu {
    private Map<Integer, Command> menu;

    public Menu(Map<Integer, Command> menu) {
        this.menu = menu;
    }

    public void showMenu(){
        for (Map.Entry<Integer, Command> command : menu.entrySet()) {
            Console.writeString(command.getKey() + " - " + command.getValue());
        }
    }

    public Command getComand(int num){
        return menu.get(num);
    }
}
