package ru.rumsd.dictionary.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String readString() {
        String str = "";
        try {
            str = reader.readLine();
        } catch (IOException ignored) {

        }
        return str;
    }

    public static void writeString(String message) {
        System.out.println(message);
    }

    public static int readInt() {
        int num;
        while (true) {
            try {
                num = Integer.parseInt(readString());
                return num;
            } catch (NumberFormatException e) {
                writeString("Введено не число");
            }
        }
    }

    public static boolean saveQuestion() {
        Console.writeString("Сохранить изменения? Y/n");

        while (true) {
            String answer = Console.readString();
            if (answer.equalsIgnoreCase("y")) {
                return true;
            } else if (answer.equalsIgnoreCase("n")) {
                return false;
            } else {
                Console.writeString("Введите значение Y/n");
            }
        }
    }
}
