package ru.rumsd.dictionary.dict.model;

import ru.rumsd.dictionary.ui.Console;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Dictionary {
    private final String FILENAME;
    private final String VALIDATION_RULE;
    private final String VALIDATION_RULE_DESCRIPTION;
    private Map<String, String> data = new HashMap<>();
    private boolean changed;

    public Dictionary(String FILENAME, String VALIDATION_RULE, String VALIDATION_RULE_DESCRIPTION) {
        this.FILENAME = FILENAME;
        this.VALIDATION_RULE = VALIDATION_RULE;
        this.VALIDATION_RULE_DESCRIPTION = VALIDATION_RULE_DESCRIPTION;
        this.changed = false;
        load();
    }

    public Status create(String key, String value) {
        Status status = Validator.validate(Action.CREATE, key, VALIDATION_RULE, VALIDATION_RULE_DESCRIPTION, data);
        if (status.isValid()){
            data.put(key, value);
            changed = true;
            return new Status("Успешно добавлено в словарь");
        } else {
            return status;
        }
    }


    public Status update(String key, String newValue) {
        Status status = Validator.validate(Action.UPDATE, key, VALIDATION_RULE, VALIDATION_RULE_DESCRIPTION, data);
        if (status.isValid()){
            data.put(key, newValue);
            changed = true;
            return new Status("Слово успешно обновлено");
        } else {
            return status;
        }
    }


    public Status delete(String key) {
        Status status = Validator.validate(Action.DELETE, key, VALIDATION_RULE, VALIDATION_RULE_DESCRIPTION, data);
        if (status.isValid()){
            data.remove(key);
            changed = true;
            return new Status("Слово успешно удалено");
        } else {
            return status;
        }
    }


    public String get(String key) {
        return data.get(key);
    }


    public Map<String, String> get() {
        return new TreeMap<>(data);
    }


    public void save() {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        URL filename = cl.getResource(FILENAME);
        Console.writeString("Сохраняю в " + filename);
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename.toURI()))) {
            for (Map.Entry<String, String> entry : data.entrySet()) {
                writer.write(String.format("%s: %s%n", entry.getKey(), entry.getValue()));
            }
        } catch (IOException e) {
            System.out.println("Что то пошло не так");
        } catch (URISyntaxException ignored) {
        }
    }

    public boolean isChanged() {
        return changed;
    }

    private void load() {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        URL filename = cl.getResource(FILENAME);
        Console.writeString("Загружаю из " + filename);
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename.toURI()))) {
            while (reader.ready()) {
                String[] splitLine = reader.readLine().split(": ");
                data.put(splitLine[0], splitLine[1]);
            }
        } catch (IOException e) {
            System.out.println("Что-то пошло не так");
        } catch (URISyntaxException ignored) {
        }
    }

    @Override
    public String toString() {
        return "Словарь " + VALIDATION_RULE_DESCRIPTION;
    }
}
