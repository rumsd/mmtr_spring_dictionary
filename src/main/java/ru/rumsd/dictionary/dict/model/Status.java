package ru.rumsd.dictionary.dict.model;

public class Status {
    private String message;
    private boolean isValid;

    public Status(String message) {
        this.message = message;
    }

    public Status(String message, boolean isValid) {
        this.message = message;
        this.isValid = isValid;
    }

    public String getMessage() {
        return message;
    }

    public boolean isValid() {
        return isValid;
    }
}
