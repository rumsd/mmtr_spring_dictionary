package ru.rumsd.dictionary.dict.model;

import java.util.Map;

class Validator {

    public static Status validate(Action action, String key, String validationRule, String description, Map<String, String> data){
        boolean valid;
        boolean exist;
        switch (action){
            case CREATE:
                valid = checkValidateByRule(key, validationRule);
                exist = checkExist(key, data);
                if (valid && !exist) {
                    return new Status("Слово соответсвует правилу словаря", true);
                } else if (!valid) {
                    return new Status("Слово должно содержать " +  description);
                } else if (exist) {
                    return new Status("Слово уже присутствует в словаре", false);
                }
                break;
            case UPDATE:
                valid = checkValidateByRule(key, validationRule);
                exist = checkExist(key, data);
                if (valid && exist) {
                    return new Status("Слово соответсвует правилу словаря", true);
                } else if (!valid) {
                    return new Status("Слово должно содержать " +  description);
                } else if (!exist) {
                    return new Status("Слово отсутствует в словаре", false);
                }
            case DELETE:
                valid = checkValidateByRule(key, validationRule);
                exist = checkExist(key, data);
                if (valid && exist) {
                    return new Status("Слово соответсвует правилу словаря", true);
                } else if (!valid) {
                    return new Status("Слово должно содержать " +  description);
                } else if (!exist) {
                    return new Status("Слово отсутствует в словаре", false);
                }
        }
        return null;
    }

    private static boolean checkValidateByRule(String key, String rule){
        return key.matches(rule);
    }

    private static boolean checkExist(String key, Map<String, String> data) {
        return data.containsKey(key);
    }
}
