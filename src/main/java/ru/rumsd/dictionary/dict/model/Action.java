package ru.rumsd.dictionary.dict.model;

public enum Action {
    CREATE,
    UPDATE,
    DELETE
}
