package ru.rumsd.dictionary.dict.view;

import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.dict.model.Status;

import java.util.Map;

@Component("view")
public interface View {
    void render(String line);

    void render(Map<String, String> data);

    void renderStatus(Status status);
}
