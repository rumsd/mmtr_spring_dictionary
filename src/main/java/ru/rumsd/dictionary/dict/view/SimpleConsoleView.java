package ru.rumsd.dictionary.dict.view;

import org.springframework.stereotype.Component;
import ru.rumsd.dictionary.dict.model.Status;
import ru.rumsd.dictionary.ui.Console;

import java.util.Map;

@Component
public class SimpleConsoleView implements View {

    @Override
    public void render(String line) {
        printBefore();
        Console.writeString(line);
        printAfter();
    }

    @Override
    public void render(Map<String, String> data) {
        printBefore();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            Console.writeString(entry.getKey() + ": " + entry.getValue());
        }
        printAfter();
    }

    @Override
    public void renderStatus(Status status) {
        printBefore();
        System.out.println(status.getMessage());
        printAfter();
    }

    private void printAfter() {
        Console.writeString("");
        Console.writeString("*************************************");
        Console.writeString("");
    }

    private void printBefore() {
        Console.writeString("*************************************");
        Console.writeString("");
    }
}
