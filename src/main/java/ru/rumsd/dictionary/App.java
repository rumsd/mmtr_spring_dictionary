package ru.rumsd.dictionary;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.rumsd.dictionary.ui.Console;
import ru.rumsd.dictionary.ui.Menu;
import ru.rumsd.dictionary.ui.command.Command;

public class App {
    public final static ApplicationContext context = new ClassPathXmlApplicationContext("config/dictionary_anotation.xml");

    public static void main(String[] args) {
        ((Command) context.getBean("startMenu")).execute();
        Menu menu = (Menu) context.getBean("menu");

        while (true) {
            menu.showMenu();
            int commandNum = Console.readInt();
            Command command = menu.getComand(commandNum);
            if (command != null) {
                command.execute();
            }
        }

    }
}
